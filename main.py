import time
import math

class Map:

    def __init__(self):
        print('Podaj rozmiar mapy')
        self.size_x = input('x= ')
        self.size_y = input('y= ')

    def change_size(self):
        self.size_x = input('x= ')
        self.size_y = input('y= ')

    def print_size(self):
        print('x = {}'.format(self.size_x))
        print('y = {}'.format(self.size_y))

    def get_size_x(self):
        return self.size_x

    def get_size_y(self):
        return self.size_y


class Samolot():

    def __init__(self):
        print('Podaj wspolrzedne poczatkowe samolotu')
        self.x = float(input('x='))
        self.y = float(input('y='))
        print('Podaj predkosc poczatkowa samolotu')
        self.v = input('v= ')


    def print_coordinates(self):
        print('x = {}'.format(self.x))
        print('y = {}'.format(self.y))

    def fly(self, rx, ry, vx, vy):
        self.last_time = time.time() #czas odnotowania pozycji
        while self.x < rx and self.y < ry:#jezeli jest w obrebie punktu
            self.x = self.x + vx * (time.time()-self.last_time) #zmiana pozycji x
            self.y = self.y + vy * (time.time() - self.last_time) #zmiana pozycji y
            print(self.x, self.y, self.last_time)
            time.sleep(0.1)


class Trasa():



    def __init__(self, a, b):
        self.l = 1 + int(input('Liczba punktow: '))  # z ilu punktow sklada sie trasa + punkt poczatkowy
        self.lista = [(a, b)]#na pierwsze miejsce trasy przyjmuje wspolrzedne poczatkowe samolotu
        for i in range(1, self.l):
            print(i)
            self.lista.append((input('x= '), input('y= ')))#jakie sa wspolrzedne tych ponktow
            print('\n')

    def plan_trasy(self, v): #tworzy liste kolejnych wektorow predkosci
        self.vx=[0]
        self.vy=[0]

        for i in range(self.l-1): #ostatni punkt jest punktem koncowym dlatego (l-1) POPRAWIC  MA ROZMIAR LISTY
            self.xd = (self.lista[i+1][0]-self.lista[i][0]) #odleglosc na kierunku x pom. punk. trasy
            self.yd = (self.lista[i + 1][1] - self.lista[i][1])  #odleglosc na kierunku y pom. punk. trasy
            self.m = math.sqrt(pow(self.xd,2)+pow(self.yd,2)) #dlugosc wektora pomiedzy punktami trasy
            self.sin =(self.xd / self.m)
            self.cos = (self.yd / self.m)
            self.vx.insert(i, self.sin * v) #predkosc x od danego odcinka trasy
            self.vy.insert(i, self.cos * v)# predkosc od danego odcinka trasy
            print(i, self.vx[i], self.vy[i])


map1 = Map()
samolot = Samolot()
samolot.print_coordinates()
trasa = Trasa(samolot.x,samolot.y)
trasa.plan_trasy(samolot.v)
for i in range(trasa.l-1):
    print('vx={}  vy={}'.format(trasa.vx[i], trasa.vy[i]))
    samolot.fly(trasa.lista[i+1][0], trasa.lista[i+1][1],trasa.vx[i],trasa.vy[i])
    print('\n x = {}   y = {} '.format(samolot.x, samolot.y))